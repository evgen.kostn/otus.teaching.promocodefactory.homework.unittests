﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        /// <summary>
        /// Метод для создания тестового Партнера
        /// </summary>
        /// <returns>Тестовый партнер</returns>
        public Partner CreateBasePartner()
        {
            // в методе по умолчанию не создавался Id партнера и сам партнер не был связан внутри с лимитом промокода

            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "СуперЭльДнс",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };

            var newLimit = new PartnerPromoCodeLimit()
            {
                Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                CreateDate = new DateTime(2020, 07, 9),
                EndDate = new DateTime(2020, 10, 9),
                Limit = 100,
                Partner = partner,
                PartnerId = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8")
            };

            partner.PartnerLimits.Add(newLimit);

            return partner;
        }

        /// <summary>
        /// Метод для создания тестового лимита промокодов
        /// </summary>
        /// <returns>Тестовый лимит промокодов</returns>
        public SetPartnerPromoCodeLimitRequest CreateBasePartnerPromoCodeNewLimit()
        {
            var newLimit = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 100,
                EndDate = new DateTime(2023, 12, 31)
            };

            return newLimit;
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            Partner partner = null;
            var newLimit = CreateBasePartnerPromoCodeNewLimit();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            // --- Передаем null и проверяем возвращение ошибки 404
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, newLimit);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var partner = CreateBasePartner();
            partner.IsActive = false;
            var newLimit = CreateBasePartnerPromoCodeNewLimit();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            // --- Блокируем партнера и проверяем возвращение ошибки 400
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, newLimit);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ResetNumberIssuedPromoCodesByReachingLimits_ReturnTrue()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var partner = CreateBasePartner();
            var newLimit = CreateBasePartnerPromoCodeNewLimit();
            bool result = false;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            // Проверяем что количество выданных промокодов обнулись успешно
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, newLimit);
            result = partner.NumberIssuedPromoCodes == 0;

            // Assert

            Assert.True(result);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_CancelLastLimitByReachingLimits_ReturnTrue()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var partner = CreateBasePartner();
            var newLimit = CreateBasePartnerPromoCodeNewLimit();
            bool result = false;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            // Проверяем что предпоследний лимит успешно обнулился,
            // для этого берем предпоследний элемент из списка лимитов -
            // проверяем что дата отмены лимита не пуста (последний в списке это только что успешно созданный новый лимит)
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, newLimit);

            var newLimitsList = partner.PartnerLimits.ToList().SkipLast(1);
            if ( newLimitsList.Last().CancelDate != null )
                result = true;

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewLimitIsEqualLessZero_ReturnBadRequest()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var partner = CreateBasePartner();

            var newLimit = CreateBasePartnerPromoCodeNewLimit();
            newLimit.Limit = 0;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            // Проверяем что новый лимит не равен 0 - выпадет BadRequest в случае <= 0
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, newLimit);

            // Assert

            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewPromocodeLimitSaved_ReturnOk()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var partner = CreateBasePartner();
            var newLimit = CreateBasePartnerPromoCodeNewLimit();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            // Проверяем что лимит сохранился посредством запроса ID этого лимита через метод контроллера,
            // если метод отработал успешно - значит промокод найден в БД
            var setPartnerLimitTask = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, newLimit);

            var lastLimitId = partner.PartnerLimits.Last().Id;
            var checkNewLimitExistingTask = await _partnersController.GetPartnerLimitAsync(partnerId, lastLimitId);
            var checkResult = checkNewLimitExistingTask.Result;

            // Assert
            checkResult.Should().BeAssignableTo<OkObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewPromocodeLimitPartnerLinkSaved_ReturnTrue()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var partner = CreateBasePartner();
            var newLimit = CreateBasePartnerPromoCodeNewLimit();
            var result = false;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            // Проверяем новый линк лимита промокода связан с неким партнером
            var setPartnerLimitTask = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, newLimit);

            var lastLimit = partner.PartnerLimits.Last();
            result = lastLimit.Partner != null;

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_NewPromocodeLimitIdCreated_ReturnTrue()
        {
            // Arrange
            var partnerId = Guid.NewGuid();
            var partner = CreateBasePartner();
            var newLimit = CreateBasePartnerPromoCodeNewLimit();
            var result = false;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            // Проверяем что Id лимита был создан посредством проверки ID созданного последним лимита
            var setPartnerLimitTask = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, newLimit);
            var lastLimitId = partner.PartnerLimits.Last().Id;
            result = lastLimitId != null && lastLimitId != Guid.Empty;

            // Assert
            Assert.True(result);
        }
    }
}